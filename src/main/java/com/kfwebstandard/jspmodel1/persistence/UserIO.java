package com.kfwebstandard.jspmodel1.persistence;

import java.io.IOException;
import com.kfwebstandard.jspmodel1.model.User;
import java.io.BufferedWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * To keep the examples simple persistence is achieved by writing to a text file
 * rather than to a database. It could be replaced with a Data Access Object
 * (DAO) without the need to modify anything else due to adherence to the
 * principle of Separation of Concerns (S0C)
 *
 * @author Ken Fogel
 */
public class UserIO {

    // All good programmers use a Logger
    private final static Logger LOG = LoggerFactory.getLogger(UserIO.class);

    /**
     * This method writes the data in the userBean to a file. In previous years
     * I taught that it was possible to write this file into the folder that
     * contained the web application. This is no longer the case as its a bad
     * practice. You must never write into the directory structure of a web app.
     * Instead you should write to an absolute location that can be configured
     * in the web.xml file.
     *
     * The method is synchronized to make it thread safe
     *
     * Thank you to Tom Schindl @tomsontom for pointing that rather than
     * declaring the character set as Charset.forName("UTF-8") it should be
     * StandardCharsets.UTF_8
     *
     * @param userBean object that contains the data to write to the file
     * @param filename string that contains the absolute path to a file
     * @throws IOException potential error when writing to the file
     */
    public synchronized void addRecord(User userBean, String filename) throws IOException {
        LOG.debug("Filename = " + filename);
        Path emailList = Paths.get(filename);
        try (BufferedWriter writer = Files.newBufferedWriter(emailList, StandardCharsets.UTF_8, StandardOpenOption.APPEND, StandardOpenOption.CREATE)) {
            writer.write(userBean.getEmailAddress() + "|"
                    + userBean.getFirstName() + "|"
                    + userBean.getLastName() + "\n");
        }
    }

}
