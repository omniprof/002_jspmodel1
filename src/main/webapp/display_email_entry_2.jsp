<%--
    Document   : display_email_2.jsp
    Created on : 24-Jan-2014, 5:00:00 PM
    Author     : Ken

    This version uses external objects to do the work.
--%>

<%@page import="java.time.format.DateTimeFormatter"%>
<%@page import="java.time.LocalDateTime"%>
<%@page import="com.kfwebstandard.jspmodel1.persistence.UserIO"%>
<%@page import="com.kfwebstandard.jspmodel1.model.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>002 - JSP Model 1b Output</title>
        <link rel="stylesheet" href="styles/main.css" type="text/css"/>
    </head>

    <body>
        <%
            // get parameters from the request
            String firstName = request.getParameter("firstName");
            String lastName = request.getParameter("lastName");
            String emailAddress = request.getParameter("emailAddress");

            // Read the context param from web.xml
            ServletContext context = getServletContext();
            String fileName = context.getInitParameter("EmailFile");

            // use regular Java objects
            User user = new User(firstName, lastName, emailAddress);
            UserIO userIO = new UserIO();
            userIO.addRecord(user, fileName);
        %>

        <h1>002b - Thanks for joining our email list</h1>

        <p>Here is the information that you entered:</p>
        <label>Email:</label>
        <span><%= user.getEmailAddress()%></span><br>
        <label>First Name:</label>
        <span><%= user.getFirstName()%></span><br>
        <label>Last Name:</label>
        <span><%= user.getLastName()%></span><br>

        <p>To enter another email address, click on the Return button shown below.</p>

        <form action="join_email.jsp" method="post">
            <input type="submit" value="Return">
        </form>

        <p>This email address was added to our list on <%= LocalDateTime.now().format(DateTimeFormatter.ISO_DATE)%></p>

    </body>
</html>