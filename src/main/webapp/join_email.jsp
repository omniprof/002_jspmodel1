<%--
    Document   : join_email.jsp
    Created on : 24-Jan-2014, 5:00:00 PM
    Updated on : 28-Jan-2020, 12:12:00 PM
    Author     : Ken

    This file could just as easily been a plan HTML file
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>002 JSP Model 1 Input</title>
        <link rel="stylesheet" href="styles/main.css" type="text/css"/>
    </head>
    <body>
        <h1>002 - Join our email list</h1>
        <p>To join our email list, enter your name and
            email address below.</p>

        <form action="display_email_entry_1.jsp" method="get">
<!--        <form action="display_email_entry_2.jsp" method="get">-->
            <label class="pad_top">Email:</label>
            <input type="email" name="emailAddress" required><br>
            <label class="pad_top">First Name:</label>
            <input type="text" name="firstName" required><br>
            <label class="pad_top">Last Name:</label>
            <input type="text" name="lastName" required><br>
            <label>&nbsp;</label>
            <input type="submit" value="Join Now" class="margin_left">
        </form>
    </body>
</html>