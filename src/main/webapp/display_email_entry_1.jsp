<%--
    Document   : display_email_1.jsp
    Created on : 24-Jan-2014, 5:00:00 PM
    Author     : Ken

    This version stores all the code in the JSP
--%>

<%@page import="java.time.format.DateTimeFormatter"%>
<%@page import="java.time.LocalDateTime"%>
<%@page import="java.io.BufferedWriter"%>
<%@page import="java.io.IOException"%>
<%@page import="com.kfwebstandard.jspmodel1.model.User"%>
<%@page import="java.nio.charset.Charset"%>
<%@page import="java.nio.file.StandardOpenOption"%>
<%@page import="java.nio.file.Files"%>
<%@page import="java.nio.file.Paths"%>
<%@page import="java.nio.file.Path"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>002 - JSP Model 1a  Output</title>
        <link rel="stylesheet" href="styles/main.css" type="text/css"/>
    </head>

    <body>
        <%!
            // declare a method for the page
            public synchronized void addRecord(User userBean, String filename) throws IOException {
                //Retrive the context-param with the file path
                Path emailList = Paths.get(filename);
                try (BufferedWriter writer = Files.newBufferedWriter(emailList, Charset.forName("UTF-8"), StandardOpenOption.APPEND, StandardOpenOption.CREATE)) {
                    writer.write(userBean.getEmailAddress() + "|"
                            + userBean.getFirstName() + "|"
                            + userBean.getLastName() + "\n");
                }
            }

        %>
        <%
            String firstName = request.getParameter("firstName");
            String lastName = request.getParameter("lastName");
            String emailAddress = request.getParameter("emailAddress");

            // Read the context param from web.xml
            ServletContext context = getServletContext();
            String fileName = context.getInitParameter("EmailFile");

            User user = new User(firstName, lastName, emailAddress);

            // use the declared method
            this.addRecord(user, fileName);
        %>

        <h1>002a - Thanks for joining our email list</h1>

        <p>Here is the information that you entered:</p>
        <label>Email:</label>
        <span><%= user.getEmailAddress()%></span><br>
        <label>First Name:</label>
        <span><%= user.getFirstName()%></span><br>
        <label>Last Name:</label>
        <span><%= user.getLastName()%></span><br>

        <p>To enter another email address, click on the Return button shown below.</p>

        <form action="join_email.jsp" method="post">
            <input type="submit" value="Return">
        </form>

        <p>This email address was added to our list on <%= LocalDateTime.now().format(DateTimeFormatter.ISO_DATE) %></p>

    </body>
</html>